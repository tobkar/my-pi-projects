#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import * 
from time import sleep, strftime
from datetime import datetime
import time

lcd = Adafruit_CharLCD()

cmd = "ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1"

lcd.begin(16,1)

def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output

t0=time.time()

while 1:
        lcd.home()
        ipaddr = run_cmd(cmd)
        t1 = str(time.time()-t0)
        lcd.message(t1+"\n")
        #seconds = int(t1[0:(t1.find("."))])
        seconds = 240*60
        lcd.message("sec: %d" % seconds)
        sleep(0.1)
