#!/usr/bin/python

import os
import glob
import RPi.GPIO as GPIO
from time import sleep
from Lcd import HD44780
import time


os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
 
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'
#BtBack = 100
BtEnter = 9
BtMore = 10
BtLess = 11
Led1 = 8
Led2 = 7
Led3 = 25

GPIO.setmode(GPIO.BCM)
# ---- LEDS ----
GPIO.setup(Led2, GPIO.OUT) ## LED "2" red
GPIO.output(Led2,False)
GPIO.setup(Led1, GPIO.OUT) ## LED "1" (red)
GPIO.output(Led1,False)
GPIO.setup(Led3, GPIO.OUT) ## LED "3" (yellow)
GPIO.output(Led3,False)
# ---- Buttons ----
GPIO.setup(BtEnter,GPIO.IN) ## Button "2"
GPIO.setup(BtMore,GPIO.IN) ## Button "3"
GPIO.setup(BtLess,GPIO.IN) ## Button "4"

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines
 
def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c

if __name__ == '__main__':
    noTemp = True
    cSec = 3
    lcd = HD44780()
    lcd_line1 = "Beer machine 1.0"
    lcd_line2_b1 = "Meny"
    lcd_line2_b2 = "Strt"
    lcd_line2_b3 = " <+>"
    lcd_line2_b4 = " <->"
    lcd.message(lcd_line1)
    lcd.cmd(0Xc0)
    lcd.message(lcd_line2_b1+lcd_line2_b2+lcd_line2_b3+lcd_line2_b4)
    sleep(1);
    t0 = time.time()
    while True:
        if noTemp:
            if cSec >= 3: 
                temp = read_temp()
                if temp = -100:
                    noTemp = True
                else:
                    noTemp = False
        if time.time()-t0>=0.25:
            if temp >= 30:
                GPIO.output(Led2,True)
            if temp < 30:
                GPIO.output(Led2,False)
            if GPIO.input(BtEnter):
                lcd_line2_b2 = "Strt"
            else:
                lcd_line2_b2 = " ** "
            if GPIO.input(BtMore):
                lcd_line2_b3 = " <+>"
                GPIO.output(Led1,False)
            else:
                lcd_line2_b3 = " ** "
                GPIO.output(Led1,True)
            if GPIO.input(BtLess):
                lcd_line2_b4 = "<-> "
                GPIO.output(Led3, False)
            else:
                lcd_line2_b4 = "  **"
                GPIO.output(Led3, True)
            
            lcd.cr()
            lcd.message("Temp:%3.3f C" % temp)
            lcd.cmd(0XC0)
            lcd.message(lcd_line2_b1+lcd_line2_b2+lcd_line2_b3+lcd_line2_b4)
            t0 = time.time()
        
        
      
        

